#ifndef _PASTIX_CONFIG_H_
#define _PASTIX_CONFIG_H_

#define PASTIX_VERSION_MAJOR @PASTIX_VERSION_MAJOR@
#define PASTIX_VERSION_MINOR @PASTIX_VERSION_MINOR@
#define PASTIX_VERSION_MICRO @PASTIX_VERSION_MICRO@

#cmakedefine PASTIX_WITH_MPI
#cmakedefine PASTIX_WITH_CUDA
#cmakedefine PASTIX_WITH_EZTRACE

#cmakedefine PASTIX_DUMP_CBLK

/* system */
#cmakedefine HAVE_PTHREAD
#cmakedefine HAVE_SCHED_SETAFFINITY
#cmakedefine HAVE_CLOCK_GETTIME
/* #cmakedefine HAVE_ASPRINTF */
/* #cmakedefine HAVE_VASPRINTF */
#cmakedefine HAVE_ATOMIC_GCC_32_BUILTINS
#cmakedefine HAVE_ATOMIC_GCC_64_BUILTINS
#cmakedefine HAVE_ATOMIC_GCC_128_BUILTINS
#cmakedefine HAVE_ATOMIC_XLC_32_BUILTINS
#cmakedefine HAVE_ATOMIC_XLC_64_BUILTINS
#cmakedefine HAVE_ATOMIC_MIPOSPRO_32_BUILTINS
#cmakedefine HAVE_ATOMIC_MIPOSPRO_64_BUILTINS
#cmakedefine HAVE_ATOMIC_SUN_32
#cmakedefine HAVE_ATOMIC_SUN_64
#cmakedefine HAVE_STDARG_H
#cmakedefine HAVE_UNISTD_H
#cmakedefine HAVE_VA_COPY
#cmakedefine HAVE_UNDERSCORE_VA_COPY
#cmakedefine HAVE_GETOPT_LONG
#cmakedefine HAVE_GETRUSAGE
#cmakedefine HAVE_GETOPT_H
#cmakedefine HAVE_ERRNO_H
#cmakedefine HAVE_STDDEF_H
#cmakedefine HAVE_LIMITS_H
#cmakedefine HAVE_STRING_H
#cmakedefine HAVE_COMPLEX_H

/* Architecture */
#cmakedefine PASTIX_ARCH_X86
#cmakedefine PASTIX_ARCH_X86_64
#cmakedefine PASTIX_ARCH_PPC
#cmakedefine PASTIX_OS_MACOS

/* Optional packages */
#cmakedefine HAVE_HWLOC
#cmakedefine HAVE_HWLOC_BITMAP
#cmakedefine HAVE_HWLOC_PARENT_MEMBER
#cmakedefine HAVE_HWLOC_CACHE_ATTR
#cmakedefine HAVE_HWLOC_OBJ_PU

/* Ordering options */
#cmakedefine PASTIX_ORDERING_SCOTCH
#cmakedefine PASTIX_ORDERING_METIS
#cmakedefine PASTIX_ORDERING_PTSCOTCH

/* Symbolic factorization options */
#cmakedefine PASTIX_SYMBOL_FORCELOAD
#cmakedefine PASTIX_SYMBOL_DUMP_SYMBMTX

#cmakedefine PASTIX_BLEND_GENTRACE

#cmakedefine FORGET_PARTITION
#cmakedefine COMPACT_SMX

/* Scheduling options */
#cmakedefine PASTIX_WITH_PARSEC
#cmakedefine PASTIX_WITH_STARPU
#cmakedefine PASTIX_WITH_STARPU
#cmakedefine PASTIX_WITH_STARPU_PROFILING

#cmakedefine PASTIX_CUDA_FERMI

#cmakedefine PASTIX_FUNNELED
#cmakedefine PASTIX_DISTRIBUTED
#cmakedefine PASTIX_THREAD_COMM

/* Debug */
#cmakedefine PASTIX_DEBUG_GRAPH
#cmakedefine PASTIX_DEBUG_SYMBOL
#cmakedefine PASTIX_DEBUG_BLEND
#cmakedefine PASTIX_DEBUG_FACTO
#cmakedefine PASTIX_DEBUG_PARSEC
#cmakedefine PASTIX_DEBUG_LR
#cmakedefine PASTIX_DEBUG_LR_NANCHECK

/* Datatypes used */
#cmakedefine PASTIX_INT64

#if defined(PASTIX_INT64)
#define FORCE_INT64
#define INTSSIZE64
#endif

#if defined(PASTIX_INT32)
#define FORCE_INT32
#define INTSIZE32
#endif

#if defined(PASTIX_LONG)
#define FORCE_LONG
#define LONG
#endif

#if (defined PRECISION_z || defined PRECISION_d)
#  define FORCE_DOUBLE
#  define PREC_DOUBLE
#endif
#if (defined PRECISION_z || defined PRECISION_c)
#  define FORCE_COMPLEX
#  define TYPE_COMPLEX
#endif

#if defined(PASTIX_WITH_MPI)
#define HAVE_MPI
#undef  FORCE_NOMPI
#else
#undef  HAVE_MPI
#define FORCE_NOMPI
#endif

#if defined(PASTIX_ORDERING_SCOTCH)
#define HAVE_SCOTCH
#define WITH_SCOTCH
#endif

#if defined(PASTIX_ORDERING_METIS)
#define HAVE_METIS
#endif

#if defined(PASTIX_ORDERING_PTSCOTCH)
#define HAVE_PTSCOTCH
#endif

/*
 * BEGIN_C_DECLS should be used at the beginning of your declarations,
 * so that C++ compilers don't mangle their names.  Use END_C_DECLS at
 * the end of C declarations.
 */
#undef BEGIN_C_DECLS
#undef END_C_DECLS
#if defined(c_plusplus) || defined(__cplusplus)
# define BEGIN_C_DECLS extern "C" {
# define END_C_DECLS }
#else
#define BEGIN_C_DECLS          /* empty */
#define END_C_DECLS            /* empty */
#endif

#endif  /* _PASTIX_CONFIG_H_ */
