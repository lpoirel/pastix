/**
 *
 *  PaStiX is a software package provided by Inria Bordeaux - Sud-Ouest,
 *  LaBRI, University of Bordeaux 1 and IPB.
 *
 * @version 1.0.0
 * @author Mathieu Faverge
 * @author Pierre Ramet
 * @author Xavier Lacoste
 * @date 2011-11-11
 * @precisions normal z -> c d s
 *
 **/
/* 
   File: z_sopalin_option.c

   Implements a function tha will print PaStiX
   compilation option.
*/
/*********************************/
/*
  Function: z_sopalin_option
   
  Print PaStiX compile options.

  Parameters:

  Returns:
    void
 */
/*********************************/
void z_sopalin_option(void );
